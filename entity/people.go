package entity

type People struct {
	Name     string `json: "name"`
	LastName string `json: "last-name"`
}

var peoples []People

func (p People) FindAll() []People {
	return peoples

}
