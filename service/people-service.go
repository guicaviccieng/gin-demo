package service

import "demo.com/gin-demo/entity"

type PeopleService interface {
	Save(entity.People) entity.People
	FindAll() []entity.People
}

type peopleService struct {
	peoples []entity.People
}

func (service *peopleService) Save(people entity.People) entity.People {
	service.peoples = append(service.peoples, people)
	return people

}

func (service *peopleService) FindAll() []entity.People {
	return service.peoples
}
