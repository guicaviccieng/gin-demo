package main

import (
	"fmt"

	"demo.com/gin-demo/controller"
	"demo.com/gin-demo/entity"
	"demo.com/gin-demo/service"
	"github.com/gin-gonic/gin"
)

var (
	videoService    service.VideoService       = service.New()
	videoController controller.VideoController = controller.New(videoService)
)

func main() {
	fmt.Println("Run Api!")

	server := gin.Default()
	// server.GET("/status", func(c *gin.Context) {
	// 	c.JSON(200, gin.H{
	// 		"message": "ok",
	// 	})
	// })

	server.GET("/videos", func(ctx *gin.Context) {
		ctx.JSON(200, videoController.FindAll())
	})

	server.POST("/videos", func(ctx *gin.Context) {
		// log.Fatal("CONTEXTO Header")
		fmt.Println("CONTEXTO Body")
		var video entity.Video = entity.Video{}
		fmt.Println(video)
		json := ctx.BindJSON(ctx.Request.Body)
		fmt.Println("JSON BODY")
		fmt.Println(json)
		// video = ctx.Request.Body
		fmt.Println(ctx.Request.Body)
		fmt.Println("CONTEXTO Header")
		fmt.Println(ctx.Request.Header)

		ctx.JSON(200, videoController.Save(ctx))
	})

	server.Run()
}
